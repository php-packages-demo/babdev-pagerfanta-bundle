# babdev/pagerfanta-bundle

Pagination. [babdev/pagerfanta-bundle](https://packagist.org/packages/babdev/pagerfanta-bundle)

# Releases
* [*Pagerfanta Package Structure Updates*
  ](https://www.babdev.com/open-source/updates/pagerfanta-package-structure-updates)
  2020-07

# Other pagers
* See also [notes-on-computer-programming-languages/php](https://gitlab.com/notes-on-computer-programming-languages/php)
* Doctrine\ORM\Tools\Pagination
  * [php-packages-demo/doctrine-orm](https://gitlab.com/php-packages-demo/doctrine-orm)
* [paginator](https://phppackages.org/s/paginator) (PHPPackages.org)
* nette/utils
* [knplabs/knp-paginator-bundle](https://phppackages.org/p/knplabs/knp-paginator-bundle) <br/>
  ([php-packages-demo/knplabs-knp-paginator-bundle](https://gitlab.com/php-packages-demo/knplabs-knp-paginator-bundle))
* illuminate/pagination
  * https://laravel.com/api/5.3/Illuminate/Pagination.html
  * https://laravel-guide.readthedocs.io/en/latest/pagination/
* knplabs/knp-components
* laminas/laminas-paginator
